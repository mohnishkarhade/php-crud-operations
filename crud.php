<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PHP Tutorials By Mohnish Karhade</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      h1{
        font-family: 'Century Gothic';
        color: #dc2b2b;
      }
    </style>
  </head>
  <body>
    <h1 class="text-center">CRUD OPERATIONS</h1>
    <br>

    <div class="container">
      <div class="row">
        <div class="col-sm-5">
          <div class="panel panel-default">
            <div class="panel-heading">
              <p class="panel-text"> INSERT DATA</p>
            </div>
            <div class="panel-body">
              <form action="insert.php" method="post">
                <div class="form-group">
                  <label for="fullname"> Your Fullname:</label>
                  <input type="text" class="form-control" id="fullname" name="fullname" required="true">
                </div>

                <div class="form-group">
                  <label for="email"> Email Id:</label>
                  <input type="email" class="form-control" id="email" name="email" required="true">
                </div>

                <div class="form-group">
                  <label for="phone"> Phone No:</label>
                  <input type="number" class="form-control" id="phone" name="phone" required="true">
                </div>

                <div class="form-group">
                  <label for="message"> Message:</label>
                  <textarea class="form-control" rows="5" required="true" id="message" name="message"> </textarea>
                </div>

                <button type="submit" value="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Insert</button>

                <button type="reset" value="reset" class="btn btn-default"><i class="fa fa-repeat"></i> Reset</button>
              </form>
            </div>
          </div>
        </div>

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <p class="panel-text">VIEW DATA</p>
            </div>
            <div class="panel-body">
              <table class="tabel table">
                <thead>
                  <tr><th>ID</th> <th>Fullname</th> <th>Email</th> <th>Phone</th> <th>Message</th> <th></th></tr>
                </thead>
                <tbody>
                  <?php
                    include("conn.php");
                    $sql = ("SELECT * FROM crud_data ORDER BY id ASC");
                    $result = mysqli_query($conn, $sql);

                    while ($row = mysqli_fetch_row($result)) {
                      $id = $row[0];
                   ?>

                   <tr>
                     <td><?php echo $row[0]; ?></td>
                     <td><?php echo $row[1]; ?></td>
                     <td><?php echo $row[2]; ?></td>
                     <td><?php echo $row[3]; ?></td>
                     <td><?php echo $row[4]; ?></td>
                     <td>
                       <a href='delete.php?id=<?php echo $id; ?>' class="btn btn-danger"> <i class="fa fa-trash"></i> Delete</a> &nbsp;
                       <a href='update.php?id=<?php echo $id; ?>' class="btn btn-info"> <i class="fa fa-pencil-square-o"></i> Update</a>
                     </td>
                   </tr>

                   <?php
                 }
                    ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
