<?php

  if (isset($_POST['total'])) {
    $a = $_POST['a'];
    $b = $_POST['b'];
    $t = $a+$b;
  }
 ?>

 <!DOCTYPE html>
 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
     <title>PHP Tutorials By Mohnish Karhade</title>

     <!-- Bootstrap -->
     <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.min.css" rel="stylesheet">

     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
     <style>
       h1{
         font-family: 'Century Gothic';
         color: #dc2b2b;
       }
     </style>
   </head>
   <body>
     <h1 class="text-center"><i class="fa fa-user fa-lg"></i> SUM OF TWO NUMBERS</h1>
     <br>

     <div class="container">
       <div class="row">
         <div class="col-sm-4">
           <div class="panel panel-default">
             <div class="panel-heading">
               <p class="panel-text">Input</p>
             </div>
             <div class="panel-body">
               <form method="post">
                 <div class="input-group">
                   <label for="num1">Input A:</label>
                   <input class="form-control" name="a" id="num1" type="text">
                 </div>

                 <div class="input-group">
                   <label for="num2">Input B:</label>
                   <input class="form-control" name="b" id="num2" type="text">
                 </div>
                 <br />
                 <button type="submit" class="btn btn-success" name="total">Total</button>
               </form>
             </div>
             <div class="panel-footer">
               <p class="text-success">Total is <label class="label label-danger"><?php echo $t; ?></label></p>
             </div>
           </div>
         </div>
       </div>
     </div>

     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     <!-- Include all compiled plugins (below), or include individual files as needed -->
     <script src="js/bootstrap.min.js"></script>
   </body>
 </html>
