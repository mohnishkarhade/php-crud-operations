<?php
  include 'conn.php';

  $id = $_REQUEST['id'];

  $sql = ("SELECT * FROM crud_data WHERE id = '$id'");
  $result = mysqli_query($conn, $sql);

  $row = mysqli_fetch_array($result);
  if (!$row) {
    die ("No record found");
  }

    $fullname = $row['1'];
    $email = $row['2'];
    $phone = $row['3'];
    $message = $row['4'];

  if (isset($_POST['save'])) {
    $fullname_save = $_POST['fullname'];
    $email_save = $_POST['email'];
    $phone_save = $_POST['phone'];
    $message_save = $_POST['message'];

    $sql = ("UPDATE crud_data SET fullname = '$fullname_save', email = '$email_save', phone = '$phone_save', message = '$message_save' WHERE id = '$id'");

    $result = mysqli_query($conn, $sql);

    if (!result) {
      die ("Error:" . mysqli_error($result));
    }

    header("location:crud.php");

    mysqli_close($conn);
  }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PHP Tutorials By Mohnish Karhade</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      h1{
        font-family: 'Century Gothic';
        color: #dc2b2b;
      }
    </style>
  </head>
  <body>
    <h1 class="text-center">CRUD OPERATIONS</h1>
    <br>

    <div class="container">
      <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <p class="panel-text"> UPDATE DATA</p>
            </div>
            <div class="panel-body">
              <form method="post">
                <div class="form-group">
                  <label for="fullname"> Your Fullname:</label>
                  <input type="text" class="form-control" id="fullname" name="fullname" required="true" value="<?php echo $fullname ?>">
                </div>

                <div class="form-group">
                  <label for="email"> Email Id:</label>
                  <input type="email" class="form-control" id="email" name="email" required="true" value="<?php echo $email ?>">
                </div>

                <div class="form-group">
                  <label for="phone"> Phone No:</label>
                  <input type="number" class="form-control" id="phone" name="phone" required="true" value="<?php echo $phone ?>">
                </div>

                <div class="form-group">
                  <label for="message"> Message:</label>
                  <textarea class="form-control" rows="5" required="true" id="message" name="message" value=""><?php echo $message ?> </textarea>
                </div>

                <button type="submit" value="save" name="save" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Update</button>

                <a href="crud.php" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
              </form>
            </div>
          </div>
        </div>
        <div class="col-sm-3"></div>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
