<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PHP Tutorials By Mohnish Karhade</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      h1{
        font-family: 'Century Gothic';
        color: #dc2b2b;
      }
    </style>
  </head>
  <body>
    <h1 class="text-center"><i class="fa fa-user fa-lg"></i> MOHNISH KARHADE</h1>
    <br>

    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <p class="panel-text">List of Basic Tutorials</p>
            </div>
            <div class="panel-body">
              <div class="list-group">
                <a href="crud.php" class="list-group-item">CRUD OPERATIONS IN PHP</a>
                <a href="sum.php" class="list-group-item">Sum of two numbers.</a>
                <a href="#" class="list-group-item"></a>
                <a href="#" class="list-group-item"></a>
                <a href="#" class="list-group-item"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
